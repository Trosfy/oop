<?php
    require_once "animal.php";
    class Frog extends animal{
        public function __construct($name)
        {
            $this->name = $name;
            $this->legs = 4;
            $this->cold_blooded = false;
        }
        public function jump(){
            echo "hop hop<br>";
        }
    }
?>