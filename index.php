<?php
    require_once "animal.php";
    require_once "Frog.php";
    require_once "Ape.php";
    $sheep = new Animal("shaun");

    echo $sheep->get_name().'<br>'; // "shaun"
    echo $sheep->get_legs().'<br>'; // 2
    echo $sheep->get_cold_blooded()?"true<br>":"false<br>"; // false

    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
    // index.php
    $sungokong = new Ape("kera sakti");
    $sungokong->yell(); // "Auooo"

    $kodok = new Frog("buduk");
    $kodok->jump() ; // "hop hop"

?>